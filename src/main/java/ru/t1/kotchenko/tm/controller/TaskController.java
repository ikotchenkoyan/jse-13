package ru.t1.kotchenko.tm.controller;

import ru.t1.kotchenko.tm.api.controller.ITaskController;
import ru.t1.kotchenko.tm.api.service.ITaskService;
import ru.t1.kotchenko.tm.enumerated.Sort;
import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.model.Task;
import ru.t1.kotchenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService.findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASKS CLEAR]");
        taskService.deleteAll();
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(index);
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        showTask(task);
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
    }

    @Override
    public void showTaskByProjectId() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        renderTasks(tasks);
    }

    private void renderTasks(List<Task> tasks) {
        System.out.println("[SHOW TASKS]");
        int index = 1;
        for (final Task task : tasks) {
            final String name = task.getName();
            final String description = task.getDescription();
            System.out.printf("%s. %s : %s \n", index, name, description);
            index++;
        }
    }

    @Override
    public void startTaskById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeTaskById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusName = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusName);
        taskService.changeTaskStatusById(id, status);
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusName = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusName);
        taskService.changeTaskStatusByIndex(index, status);
    }
}
