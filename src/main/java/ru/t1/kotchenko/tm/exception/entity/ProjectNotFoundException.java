package ru.t1.kotchenko.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityExceotion {

    public ProjectNotFoundException() {
        super("Error! Project not found.");
    }

}
