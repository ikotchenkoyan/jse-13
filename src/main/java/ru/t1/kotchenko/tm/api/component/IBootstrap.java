package ru.t1.kotchenko.tm.api.component;

public interface IBootstrap {

    void run(String... args);

}
