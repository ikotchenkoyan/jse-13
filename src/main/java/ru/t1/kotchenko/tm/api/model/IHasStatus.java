package ru.t1.kotchenko.tm.api.model;

import ru.t1.kotchenko.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
